// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair


#include <assert.h>
#include "Collatz.h"

using namespace std;

int cache[1000000];


// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// cycle_length
// ------------
int cycle_length (long val) {
    // base case
    if (val == 1) {
        return 1;
    } else if (val < 1000000 && cache[val]) {
        return cache[val];
    } else {
        // not cached
        if (val < 1000000) {
            assert(cache[val] == 0);
        }
        int cur_cl;
        if (val%2 == 0) {
            long next = val / 2;
            cur_cl = 1 + cycle_length(next);
        } else {
            long next = 3 * val + 1;
            cur_cl = 1 + cycle_length(next);
        }
        // cache the result that's been calculated
        if (val < 1000000) {
            cache[val] = cur_cl;
        }
        return cur_cl;
    }
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    if (i > j) {
        int temp = i;
        i = j;
        j = temp;
    }
    assert (i <= j);
    assert (i < 1000000 && j < 1000000);
    if (i < j/2 + 1) {
        int m = j/2 + 1;
        return collatz_eval(m, j);
    }
    assert (i >= j/2 + 1);
    int max = 0;
    long val;
    for (int idx = i; idx <= j; ++idx) {
        val = idx;
        int cl = cycle_length(val);
        if (cl > max) {
            max = cl;
        }
    }
    return max;
}




// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
