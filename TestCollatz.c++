// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----


TEST(CollatzFixture, read_1) {
    string s("10 100\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   10);
    ASSERT_EQ(p.second, 100);
}

TEST(CollatzFixture, read_2) {
    string s("100 1000\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   100);
    ASSERT_EQ(p.second, 1000);
}

TEST(CollatzFixture, read_3) {
    string s("1000 10000\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1000);
    ASSERT_EQ(p.second, 10000);
}


// ----
// eval
// ----


TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(97045, 449333);
    ASSERT_EQ(v, 449);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(487580, 8950);
    ASSERT_EQ(v, 449);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(415474, 869414);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(581749, 679516);
    ASSERT_EQ(v, 509);
}

// -----
// print
// -----

TEST(CollatzFixture, print_1) {
    ostringstream w;
    collatz_print(w, 581749, 679516, 509);
    ASSERT_EQ(w.str(), "581749 679516 509\n");
}

TEST(CollatzFixture, print_2) {
    ostringstream w;
    collatz_print(w, 263337, 373546, 441);
    ASSERT_EQ(w.str(), "263337 373546 441\n");
}

// -----
// solve
// -----




TEST(CollatzFixture, solve_1) {
    istringstream r("89331 559486\n97045 449333\n487580 8950\n415474 869414\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("89331 559486 470\n97045 449333 449\n487580 8950 449\n415474 869414 525\n", w.str());
}
